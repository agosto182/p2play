package org.libre.agosto.p2play.ui.components.molecules

import android.content.Intent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.ListItemDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import coil3.compose.AsyncImage
import formatResource
import org.libre.agosto.p2play.AccountActivity
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.ReproductorActivity
import org.libre.agosto.p2play.domain.data.NotificationTypeEnum
import org.libre.agosto.p2play.models.NotificationModel

@Composable
fun NotificationItem(notification: NotificationModel, onNotificationClick: (Int) -> Unit) {
    val context = LocalContext.current
    val headlineTxt: AnnotatedString
    var leadingImage: Any? = null
    var intent: Intent? = null

    val backgroundColor = if (notification.read) {
        MaterialTheme.colorScheme.surface
    } else {
        MaterialTheme.colorScheme.primaryContainer
    }

    when (notification.type) {
        NotificationTypeEnum.NEW_VIDEO_FROM_SUBSCRIPTION.ordinal -> {
            val video = notification.video!!
            headlineTxt = buildAnnotatedString {
                append(stringResource(R.string.new_video_notification, video.channel.displayName))
                append(" ")
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(video.name)
                }
            }
            leadingImage = if (video.channel.avatars.isNotEmpty()) {
                formatResource(video.channel.avatars[0].path)
            } else {
                painterResource(R.drawable.default_avatar)
            }

            intent = Intent(context, ReproductorActivity::class.java)
            intent.putExtra("videoId", video.uuid)
        }
        NotificationTypeEnum.NEW_COMMENT_ON_MY_VIDEO.ordinal -> {
            val comment = notification.comment!!
            headlineTxt = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(comment.account.name)
                }
                append(" ")
                append(stringResource(R.string.comment_notification))
                append(" ")
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(comment.video.name)
                }
            }
            leadingImage = if (comment.account.avatars.isNotEmpty()) {
                formatResource(comment.account.avatars[0].path)
            } else {
                painterResource(R.drawable.default_avatar)
            }

            intent = Intent(context, ReproductorActivity::class.java)
            intent.putExtra("videoId", comment.video.uuid)
        }
        NotificationTypeEnum.MY_VIDEO_PUBLISHED.ordinal,
        NotificationTypeEnum.MY_VIDEO_IMPORT_SUCCESS.ordinal
        -> {
            val video = notification.video!!
            headlineTxt = buildAnnotatedString {
                append(stringResource(R.string.video_published_notification))
                append(" ")
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(video.name)
                }
            }
            leadingImage = painterResource(R.drawable.media3_icon_circular_play)

            intent = Intent(context, ReproductorActivity::class.java)
            intent.putExtra("videoId", video.uuid)
        }
//        NotificationTypeEnum.NEW_USER_REGISTRATION.ordinal -> {
//            val account = notification
//            headlineTxt = buildAnnotatedString {
//                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
//                    append(notification.actorFollow?.follower?.name)
//                }
//                append(" ")
//                append(stringResource(R.string.))
//            }
//            leadingImage = if (.isNotEmpty()) {
//                formatResource([0].path)
//            } else {
//                painterResource(R.drawable.default_avatar)
//            }
//        }
        NotificationTypeEnum.NEW_FOLLOW.ordinal -> {
            val actorFollow = notification.actorFollow!!
            headlineTxt = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(actorFollow.follower.name)
                }
                append(" ")
                append(stringResource(R.string.follow_notification))
                append(" ")
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(actorFollow.following.name)
                }
            }
            leadingImage = if (actorFollow.follower.avatars.isNotEmpty()) {
                formatResource(actorFollow.follower.avatars[0].path)
            } else {
                painterResource(R.drawable.default_avatar)
            }

            intent = Intent(context, AccountActivity::class.java)
            intent.putExtra("accountId", actorFollow.follower.getAccount())
        }
        NotificationTypeEnum.COMMENT_MENTION.ordinal -> {
            val comment = notification.comment!!
            headlineTxt = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(comment.account.name)
                }
                append(" ")
                append(stringResource(R.string.mention_notification))
                append(" ")
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(comment.video.name)
                }
            }
            leadingImage = if (comment.account.avatars.isNotEmpty()) {
                formatResource(comment.account.avatars[0].path)
            } else {
                painterResource(R.drawable.default_avatar)
            }
            intent = Intent(context, ReproductorActivity::class.java)
            intent.putExtra("videoId", comment.video.uuid)
        }
        else -> {
            headlineTxt = AnnotatedString(stringResource(R.string.unsupported_notification))
        }
    }

    ListItem(
        headlineContent = { Text(headlineTxt) },
        leadingContent = {
            if (leadingImage !== null) {
                // Ugly hack, fix later
                if (leadingImage::class.java.typeName != "java.lang.String") {
                    Image(
                        leadingImage as Painter,
                        null,
                        contentScale = ContentScale.Fit,
                        modifier = Modifier
                            .padding(10.dp)
                            .size(40.dp)
                            .clip(CircleShape)
                    )
                } else {
                    AsyncImage(
                        leadingImage,
                        null,
                        contentScale = ContentScale.Fit,
                        modifier = Modifier
                            .padding(10.dp)
                            .size(40.dp)
                            .clip(CircleShape)
                    )
                }
            } else {
                Icon(
                    Icons.Default.Notifications,
                    null,
                    // contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .padding(10.dp)
                        .size(40.dp)
                        .clip(CircleShape)
                        .background(Color.Gray)
                )
            }
        },
        colors = ListItemDefaults.colors(
            containerColor = backgroundColor
        ),
        modifier = Modifier.clickable {
            intent?.let {
                context.startActivity(it)
            }

            onNotificationClick(notification.id!!)
        }
    )
    HorizontalDivider()
}
