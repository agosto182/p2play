package org.libre.agosto.p2play.ui.components.molecules

import android.graphics.drawable.Icon
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp

interface ChipValues<T> {
    val text: String
    val value: T
    val icon: ImageVector?
}

@Composable
fun <T> ChipSelector(
    values: ArrayList<ChipValues<T>>,
    value: T? = null,
    modifier: Modifier = Modifier,
    callback: (T) -> Unit
) {
    Row(modifier.horizontalScroll(rememberScrollState())) {
        Spacer(modifier.width(10.dp))
        for (v in values) {
            val isSelected = value == v.value
            FilterChip(
                selected = isSelected,
                { callback(v.value) },
                label = { Text(v.text) },
                leadingIcon = {
                    if (isSelected) {
                        Icon(Icons.Default.Check, "", Modifier.height(20.dp))
                    } else if (v.icon !== null) {
                        Icon(v.icon!!, "", Modifier.height(20.dp))
                    }
                }
            )
            Spacer(modifier.width(10.dp))
        }
    }
}
