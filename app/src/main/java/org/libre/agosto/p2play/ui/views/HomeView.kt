package org.libre.agosto.p2play.ui.views

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import java.util.ArrayList
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.domain.data.VideoFilterEnum
import org.libre.agosto.p2play.ui.components.molecules.ChipSelector
import org.libre.agosto.p2play.ui.components.molecules.ChipValues
import org.libre.agosto.p2play.ui.lists.VideoList
import org.libre.agosto.p2play.viewModels.VideosViewModel

@SuppressLint("MutableCollectionMutableState")
@Composable
fun HomeView(videosViewModel: VideosViewModel, modifier: Modifier = Modifier) {
    val chips = arrayListOf(
        object : ChipValues<VideoFilterEnum> {
            override val text = stringResource(R.string.hot)
            override val value = VideoFilterEnum.HOT
            override val icon = ImageVector.vectorResource(
                R.drawable.baseline_local_fire_department_24
            )
        },
        object : ChipValues<VideoFilterEnum> {
            override val text = stringResource(R.string.nav_trending)
            override val value = VideoFilterEnum.TRENDING
            override val icon = ImageVector.vectorResource(R.drawable.ic_trending_up_black_24dp)
        },
        object : ChipValues<VideoFilterEnum> {
            override val text = stringResource(R.string.nav_likes)
            override val value = VideoFilterEnum.LIKES
            override val icon = ImageVector.vectorResource(R.drawable.ic_thumb_up_black_24dp)
        },
        object : ChipValues<VideoFilterEnum> {
            override val text = stringResource(R.string.nav_popular)
            override val value = VideoFilterEnum.POPULAR
            override val icon = Icons.Filled.Star
        },
        object : ChipValues<VideoFilterEnum> {
            override val text = stringResource(R.string.nav_recent)
            override val value = VideoFilterEnum.RECENT
            override val icon = ImageVector.vectorResource(R.drawable.ic_add_circle_black_24dp)
        },
        object : ChipValues<VideoFilterEnum> {
            override val text = stringResource(R.string.nav_local)
            override val value = VideoFilterEnum.LOCAL
            override val icon = ImageVector.vectorResource(R.drawable.ic_home_black_24dp)
        }
    )

    val videoFilter by videosViewModel.videoFilter.observeAsState(initial = VideoFilterEnum.HOT)
    val videos by videosViewModel.videos.observeAsState(initial = listOf())
    val isLoading: Boolean by videosViewModel.isLoading.observeAsState(initial = false)

    LaunchedEffect(Unit) {
        if (videos.isEmpty()) {
            videosViewModel.loadVideos()
        }
    }

    Column(modifier) {
        VideoList(
            ArrayList(videos),
            header = {
                ChipSelector(chips, videoFilter) {
                    videosViewModel.changeCategory(it)
                }
            },
            isLoading = isLoading,
            onRefresh = {
                videosViewModel.refresh()
            },
            onLoadMore = {
                if (!isLoading) {
                    videosViewModel.loadVideos()
                }
            }
        )
    }
}
