package org.libre.agosto.p2play.fragmentAdapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import org.libre.agosto.p2play.fragments.AccountChannelsFragment
import org.libre.agosto.p2play.fragments.AccountInfoFragment
import org.libre.agosto.p2play.fragments.AccountVideosFragment
import org.libre.agosto.p2play.models.AccountModel

val TAB_NAMES = arrayOf(
    "Videos",
    "Channels",
    "Info"
)

class AccountAdapter(fm: FragmentManager, c: Lifecycle) : FragmentStateAdapter(fm, c) {
    lateinit var accountId: String
    lateinit var account: AccountModel
    override fun getItemCount(): Int = 3

    override fun createFragment(i: Int): Fragment {
        val fragment = when (i) {
            0 -> AccountVideosFragment.newInstance(accountId)
            1 -> AccountChannelsFragment.newInstance(accountId)
            2 -> AccountInfoFragment.newInstance(account)
            else -> throw Error("Invalid tab")
        }

        return fragment
    }

    fun get(position: Int): CharSequence = TAB_NAMES[position]
}
