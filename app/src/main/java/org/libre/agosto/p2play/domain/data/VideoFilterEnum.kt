package org.libre.agosto.p2play.domain.data

enum class VideoFilterEnum(val filter: String) {
    TRENDING("trending"),
    HOT("hot"),
    LOCAL("local"),
    RECENT("recent"),
    LIKES("likes"),
    POPULAR("popular"),
    MY_VIDEOS("my-videos"),
    HISTORY("history")
}
