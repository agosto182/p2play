package org.libre.agosto.p2play.ui.lists

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyGridItemSpanScope
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.pulltorefresh.PullToRefreshBox
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshotFlow
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.libre.agosto.p2play.helpers.InfiniteScrollHandler
import org.libre.agosto.p2play.models.VideoModel
import org.libre.agosto.p2play.ui.components.organisms.VideoItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VideoList(
    videos: ArrayList<VideoModel>,
    header: @Composable (() -> Unit)? = null,
    isLoading: Boolean = false,
    onRefresh: (() -> Unit)? = null,
    modifier: Modifier = Modifier,
    onLoadMore: (() -> Unit)? = null
) {
    var isRefreshing by remember { mutableStateOf(false) }
    val lazyState = rememberLazyGridState()
    var spanSize = 2
    val maxSpan: (LazyGridItemSpanScope) -> GridItemSpan = {
        spanSize = it.maxCurrentLineSpan + 1
        GridItemSpan(it.maxCurrentLineSpan)
    }

    PullToRefreshBox(
        isRefreshing,
        {
            if (onRefresh !== null) {
                onRefresh()
            }
        },
        modifier
    ) {
        LazyVerticalGrid(
            GridCells.Adaptive(300.dp),
            state = lazyState,
            verticalArrangement = Arrangement.spacedBy(16.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            if (header !== null) {
                item(key = "header", span = maxSpan) { header() }
            }
            items(videos, key = { it.id }) {
                VideoItem(it)
            }
            if (isLoading) {
                item(key = "loading", span = maxSpan) {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 20.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator()
                    }
                }
            }
        }
    }

    if (onLoadMore !== null) {
        LaunchedEffect(isLoading) {
            snapshotFlow { isLoading }
                .collect {
                    if (!it) {
                        isRefreshing = false
                    }
                }
        }

        InfiniteScrollHandler(lazyState, spanSize) {
            onLoadMore()
        }
    }
}
