package org.libre.agosto.p2play.ui.views

import android.annotation.SuppressLint
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.LocalLifecycleOwner
import java.util.ArrayList
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.ui.components.molecules.LoginBanner
import org.libre.agosto.p2play.ui.lists.SubscriptionIconList
import org.libre.agosto.p2play.ui.lists.VideoList
import org.libre.agosto.p2play.viewModels.SubscriptionsViewModel

@SuppressLint("MutableCollectionMutableState")
@Composable
fun SubscriptionsView(
    subscriptionsViewModel: SubscriptionsViewModel,
    modifier: Modifier = Modifier
) {
    val lifecycleOwner = LocalLifecycleOwner.current
    val videos by subscriptionsViewModel.videos.observeAsState(initial = listOf())
    val subscriptions by subscriptionsViewModel.subscriptions.observeAsState(initial = listOf())
    val isLoading: Boolean by subscriptionsViewModel.isLoading.observeAsState(initial = false)

    var isLogged by remember { mutableStateOf(ManagerSingleton.isLogged()) }

    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, _ ->
            val oldValue = isLogged
            isLogged = ManagerSingleton.isLogged()

            if (!oldValue && isLogged) {
                if (videos.isEmpty()) {
                    subscriptionsViewModel.loadVideos()
                }

                if (subscriptions.isEmpty()) {
                    subscriptionsViewModel.loadSubscriptions()
                }
            }
        }

        lifecycleOwner.lifecycle.addObserver(observer)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    LaunchedEffect(Unit) {
        if (videos.isEmpty()) {
            subscriptionsViewModel.loadVideos()
        }

        if (subscriptions.isEmpty()) {
            subscriptionsViewModel.loadSubscriptions()
        }
    }

    if (isLogged) {
        VideoList(
            ArrayList(videos),
            {
                SubscriptionIconList(subscriptions)
            },
            isLoading = isLoading,
            onRefresh = {
                if (!isLoading) {
                    subscriptionsViewModel.refresh()
                }
            },
            onLoadMore = {
                if (!isLoading) {
                    subscriptionsViewModel.loadVideos()
                }
            }
        )
    } else {
        LoginBanner()
    }
}
