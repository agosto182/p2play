package org.libre.agosto.p2play.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.media3.exoplayer.ExoPlayer
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import org.libre.agosto.p2play.ReproductorActivity
import org.libre.agosto.p2play.activities.ui.theme.P2playTheme
import org.libre.agosto.p2play.domain.data.VideoFilterEnum
import org.libre.agosto.p2play.helpers.DetectActivity
import org.libre.agosto.p2play.singletons.PlaybackSingleton
import org.libre.agosto.p2play.ui.Routes
import org.libre.agosto.p2play.ui.bars.MainNavigationBar
import org.libre.agosto.p2play.ui.bars.MainTopAppBar
import org.libre.agosto.p2play.ui.bars.NotificationsBar
import org.libre.agosto.p2play.ui.bars.SearchTopBar
import org.libre.agosto.p2play.ui.components.molecules.Player
import org.libre.agosto.p2play.ui.views.HomeView
import org.libre.agosto.p2play.ui.views.NotificationsView
import org.libre.agosto.p2play.ui.views.SearchView
import org.libre.agosto.p2play.ui.views.SubscriptionsView
import org.libre.agosto.p2play.ui.views.VideoListView
import org.libre.agosto.p2play.ui.views.YouView
import org.libre.agosto.p2play.viewModels.NotificationsViewModel
import org.libre.agosto.p2play.viewModels.SearchViewModel
import org.libre.agosto.p2play.viewModels.SubscriptionsViewModel
import org.libre.agosto.p2play.viewModels.VideosViewModel

class MainActivity : ComponentActivity() {

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            val context = LocalContext.current
            val videoViewModel: VideosViewModel = viewModel()
            val subscriptionsViewModel: SubscriptionsViewModel = viewModel()
            val searchViewModel: SearchViewModel = viewModel()
            val videoListViewModel = VideosViewModel()
            val notificationsViewModel: NotificationsViewModel = viewModel()

            val navController = rememberNavController()
            val currentRoute = navController.currentBackStackEntryAsState().value?.destination?.route
            val routeValue = Routes.fromRoute(currentRoute)

            var player by remember { mutableStateOf<ExoPlayer?>(null) }
            val notifications by notificationsViewModel.unreadNotifications.observeAsState(listOf())

            var logCounter = 0

            LaunchedEffect(Unit) {
                navController.addOnDestinationChangedListener { _, _, _ ->
                    val routes = navController
                        .currentBackStack.value
                        .map { it.destination.route }
                        .joinToString(", ")

                    Log.d("BackStackLog", "[${logCounter++}] BackStack: $routes")
                }

                notificationsViewModel.loadMore()
            }

            DetectActivity(
                {
                    if (PlaybackSingleton.isPlaying()) {
                        player = PlaybackSingleton.player
                    }
                },
                { player = null }
            )

            P2playTheme {
                Scaffold(
                    modifier = Modifier.fillMaxSize(),
                    topBar = {
                        when (currentRoute) {
                            Routes.Videos.route -> MainTopAppBar(
                                navController,
                                Modifier,
                                notifications
                            )
                            Routes.Search.route -> SearchTopBar(navController) {
                                searchViewModel.onSearch(it)
                            }
                            Routes.Notifications.route -> {
                                NotificationsBar(navController, {
                                    notificationsViewModel.onMarkAllAsRead()
                                })
                            }
                            Routes.VideoList.route -> {}
                            else -> MainTopAppBar(navController)
                        }
                    },
                    bottomBar = {
                        when (currentRoute) {
                            Routes.VideoList.route,
                            Routes.Notifications.route
                            -> {}
                            else -> MainNavigationBar(navController)
                        }
                    }
                ) { innerPadding ->
                    var topPadding = innerPadding.calculateTopPadding()

                    routeValue?.let {
                        if (it.hasTopAppBar) {
                            topPadding = 0.dp
                        }
                    }

                    ConstraintLayout(modifier = Modifier.fillMaxSize()) {
                        val miniPlayer = createRef()
                        NavHost(
                            navController,
                            startDestination = Routes.Videos.route,
                            modifier = Modifier.padding(
                                top = topPadding,
                                bottom = innerPadding.calculateBottomPadding()
                            )
                        ) {
                            composable(Routes.Videos.route) { HomeView(videoViewModel) }
                            composable(Routes.Subscriptions.route) {
                                SubscriptionsView(subscriptionsViewModel)
                            }
                            composable(Routes.Search.route) { SearchView(searchViewModel) }
                            composable(Routes.You.route) { YouView(navController) }
                            composable(Routes.VideoList.route) { navBackStackEntry ->
                                val key =
                                    navBackStackEntry.arguments?.getString("key")
                                        ?: VideoFilterEnum.MY_VIDEOS.toString()
                                VideoListView(videoListViewModel, key, navController)
                            }
                            composable(Routes.Notifications.route) {
                                NotificationsView(notificationsViewModel)
                            }
                        }

                        if (player !== null) {
                            Box(
                                modifier = Modifier
                                    .width(150.dp)
                                    .padding(innerPadding)
                                    .padding(10.dp)
                                    .clickable {
                                        val intent =
                                            Intent(context, ReproductorActivity::class.java)
                                        intent.putExtra("resume", true)
                                        startActivity(intent)
                                    }
                                    .constrainAs(miniPlayer) {
                                        end.linkTo(parent.end)
                                        bottom.linkTo(parent.bottom)
                                    }
                            ) {
                                Player({ player })
                            }
                        }
                    }
                }
            }
        }
    }
}
