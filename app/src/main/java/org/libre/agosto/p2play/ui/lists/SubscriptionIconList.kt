package org.libre.agosto.p2play.ui.lists

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.libre.agosto.p2play.models.ChannelModel
import org.libre.agosto.p2play.ui.components.molecules.SubscriptionIcon

@Composable
fun SubscriptionIconList(subscriptions: List<ChannelModel>) {
    LazyRow(modifier = Modifier.padding(bottom = 10.dp)) {
        items(subscriptions, key = { it.id }) {
            SubscriptionIcon(it)
        }
    }
}
