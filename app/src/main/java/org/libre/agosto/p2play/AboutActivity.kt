package org.libre.agosto.p2play

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.libre.agosto.p2play.databinding.ActivityAboutBinding

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityAboutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.aboutUrl.text = "https://" + ManagerSingleton.url + "/about/instance"

        binding.aboutLabel.text =
            binding.aboutLabel.text.toString() + " " +
            this.packageManager.getPackageInfo(this.packageName, 0).versionName
    }
}
