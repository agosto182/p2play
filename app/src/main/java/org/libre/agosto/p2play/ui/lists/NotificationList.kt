package org.libre.agosto.p2play.ui.lists

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.pulltorefresh.PullToRefreshBox
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import org.libre.agosto.p2play.models.NotificationModel
import org.libre.agosto.p2play.ui.components.molecules.NotificationItem

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NotificationList(
    notification: List<NotificationModel>,
    onRefresh: () -> Unit,
    onLoadMore: () -> Unit,
    onNotificationClick: (Int) -> Unit
) {
    var isRefreshing by remember { mutableStateOf(false) }
    val lazyState = rememberLazyListState()

    PullToRefreshBox(isRefreshing, { onRefresh() }) {
        LazyColumn(state = lazyState) {
            items(notification, key = { it.id!! }) {
                NotificationItem(it, onNotificationClick)
            }
        }
    }
}
