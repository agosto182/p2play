package org.libre.agosto.p2play

import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.libre.agosto.p2play.adapters.VideosAdapter
import org.libre.agosto.p2play.ajax.Actions
import org.libre.agosto.p2play.ajax.Channels
import org.libre.agosto.p2play.ajax.Videos
import org.libre.agosto.p2play.databinding.ActivityChannelBinding
import org.libre.agosto.p2play.helpers.getViewManager
import org.libre.agosto.p2play.models.ChannelModel
import org.libre.agosto.p2play.models.VideoModel
class ChannelActivity : AppCompatActivity() {
    private lateinit var channelId: String
    private lateinit var channel: ChannelModel
    private var isSubcribed: Boolean = false
    private val channelService = Channels()
    private val videosService = Videos()
    private val actionsService = Actions()

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<VideosAdapter.ViewHolder>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var binding: ActivityChannelBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChannelBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        channelId = this.intent.extras?.getString("channel")!!

        viewManager = getViewManager(this, resources)

        binding.subcriptionBtn.setOnClickListener {
            subscribeAction()
        }
    }

    override fun onResume() {
        super.onResume()

        getChannel()
        getSubscription()
        getVideos()

        if (ManagerSingleton.user.status == 1) {
            binding.subcriptionBtn.visibility = View.VISIBLE
            getSubscription()
        }
    }

    private fun getChannel() {
        AsyncTask.execute {
            channel = channelService.getChannelInfo(channelId)
            runOnUiThread {
                binding.usernameProfile.text = channel.name
                binding.hostTxt.text = channel.host
                binding.subcriptionsTxt.text = channel.followers.toString()
                if (channel.channelImg != "") {
                    Picasso.get().load(
                        "https://${ManagerSingleton.url}${channel.channelImg}"
                    ).into(binding.channelImg)
                }
            }
        }
    }

    private fun subscribe() {
        AsyncTask.execute {
            val res = actionsService.subscribe(ManagerSingleton.token.token, channel.getFullName())
            runOnUiThread {
                if (res == 1) {
                    binding.subcriptionBtn.text = getString(R.string.unSubscribeBtn)
                    ManagerSingleton.toast(getString(R.string.subscribeMsg), this)
                    getSubscription()
                } else {
                    ManagerSingleton.toast(getString(R.string.errorMsg), this)
                }
            }
        }
    }

    private fun unSubscribe() {
        AsyncTask.execute {
            val res = actionsService.unSubscribe(
                ManagerSingleton.token.token,
                channel.getFullName()
            )
            runOnUiThread {
                if (res == 1) {
                    binding.subcriptionBtn.text = getString(R.string.subscribeBtn)
                    ManagerSingleton.toast(getString(R.string.unSubscribeMsg), this)
                    getSubscription()
                } else {
                    ManagerSingleton.toast(getString(R.string.errorMsg), this)
                }
            }
        }
    }

    private fun subscribeAction() {
        if (isSubcribed) {
            unSubscribe()
        } else {
            subscribe()
        }
    }

    private fun getSubscription() {
        AsyncTask.execute {
            isSubcribed =
                actionsService.getSubscription(ManagerSingleton.token.token, channel.getFullName())
            runOnUiThread {
                if (isSubcribed) {
                    binding.subcriptionBtn.text = getText(R.string.unSubscribeBtn)
                } else {
                    binding.subcriptionBtn.text = getText(R.string.subscribeBtn)
                }
            }
        }
    }

    private fun getVideos() {
        AsyncTask.execute {
            val videos = videosService.channelVideos(channel.getFullName(), 0)
            runOnUiThread {
                initRecycler(videos)
            }
        }
    }

    // Generic function for set data to RecyclerView
    private fun initRecycler(data: ArrayList<VideoModel>) {
        // val data = arrayListOf<VideoModel>()
        viewAdapter = VideosAdapter(data)

        recyclerView = findViewById<RecyclerView>(R.id.listVideosChannel).apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }
        // swipeContainer.isRefreshing = false
    }
}
