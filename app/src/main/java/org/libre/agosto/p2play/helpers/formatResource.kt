import org.libre.agosto.p2play.ManagerSingleton

fun formatResource(resource: String): String = "https://${ManagerSingleton.url}$resource"
