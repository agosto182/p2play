package org.libre.agosto.p2play.ajax

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import org.json.JSONObject
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.models.NotificationModel

class Notifications : Client() {
    fun get(skip: Int = 0): ArrayList<NotificationModel> {
        val url = "users/me/notifications?skip=$skip"
        val con = this.newCon(url, "GET", ManagerSingleton.token.token)
        val notifications: ArrayList<NotificationModel> = arrayListOf()

        try {
            if (con.responseCode == 200) {
                val response = InputStreamReader(con.inputStream)
                val data = JsonReader(response)
                data.beginObject()
                while (data.hasNext()) {
                    when (data.nextName()) {
                        "data" -> {
                            data.beginArray()
                            while (data.hasNext()) {
                                val notification = Gson().fromJson<NotificationModel>(
                                    data,
                                    NotificationModel::class.java
                                )
                                notifications.add(notification)
                            }
                            data.endArray()
                        }
                        else -> data.skipValue()
                    }
                }
                data.endObject()
                data.close()
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }

        return notifications
    }

    fun markAsRead(notificationId: Int): Boolean {
        val url = "users/me/notifications/read"
        val con = this.newCon(url, "POST", ManagerSingleton.token.token, true)
        con.setRequestProperty("Content-Type", "application/json")
        con.doInput = true
        val params = JSONObject()
        params.put("ids", "[$notificationId]")
        val outputStreamWriter = OutputStreamWriter(con.outputStream)
        outputStreamWriter.write(params.toString())
        outputStreamWriter.flush()

        con.responseMessage
        con.requestMethod
        try {
            if (con.responseCode == 204) {
                con.disconnect()
                return true
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }

        con.disconnect()
        return false
    }

    fun markAllAsRead(): Boolean {
        val url = "users/me/notifications/read-all"
        val con = this.newCon(url, "POST", ManagerSingleton.token.token)

        try {
            if (con.responseCode == 204) {
                con.disconnect()
                return true
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }

        con.disconnect()
        return false
    }
}
