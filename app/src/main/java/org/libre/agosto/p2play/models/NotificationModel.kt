package org.libre.agosto.p2play.models

class NotificationModel {
    val id: Int? = null
    val type: Int? = null
    val read: Boolean = false
    val createdAt: String? = null

    val video: VideoNotificationModel? = null
    val videoImport: VideoNotificationModel? = null
    val actorFollow: FollowModel? = null
    val comment: CommentNotificationModel? = null
}

data class FollowModel(val id: Int, val follower: AccountModel, val following: AccountModel)

data class VideoNotificationModel(val uuid: String, val name: String, val channel: AccountModel)

data class CommentNotificationModel(
    val threadId: Int,
    val account: AccountModel,
    val video: VideoNotificationModel
)
