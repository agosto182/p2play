package org.libre.agosto.p2play.ui.bars

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material.icons.filled.Home
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.ui.Routes

@Composable
fun MainNavigationBar(navController: NavController) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    NavigationBar {
        NavigationBarItem(
            icon = { Icon(Icons.Filled.Home, "home") },
            label = { Text(text = stringResource(R.string.nav_menu_videos), fontSize = 11.sp) },
            selected = currentDestination?.route == Routes.Videos.route,
            onClick = {
                navController.navigate(Routes.Videos.route) {
                    // Pop up to the start destination of the graph to
                    // avoid building up a large stack of destinations
                    // on the back stack as users select items
                    popUpTo(navController.graph.findStartDestination().id) {
                        saveState = true
                    }
                    // Avoid multiple copies of the same destination when
                    // reselecting the same item
                    launchSingleTop = true
                }
            }
        )
        NavigationBarItem(
            icon = { Icon(painterResource(R.drawable.ic_live_tv_black_24dp), "home") },
            label = { Text(stringResource(R.string.nav_subscriptions), fontSize = 11.sp) },
            selected = currentDestination?.route == Routes.Subscriptions.route,
            onClick = {
                navController.navigate(Routes.Subscriptions.route) {
                    // Pop up to the start destination of the graph to
                    // avoid building up a large stack of destinations
                    // on the back stack as users select items
                    popUpTo(navController.graph.findStartDestination().id) {
                        saveState = true
                    }
                    // Avoid multiple copies of the same destination when
                    // reselecting the same item
                    launchSingleTop = true
                }
            }
        )
        // NavigationBarItem(
        //     icon = { Icon(painterResource(R.drawable.ic_menu_slideshow), "home") },
        //     label = { Text(stringResource(R.string.playlists), fontSize = 11.sp) },
        //     selected = false,
        //     onClick = {}
        // )
        NavigationBarItem(
            icon = { Icon(Icons.Filled.AccountCircle, null) },
            label = { Text(stringResource(R.string.you), fontSize = 11.sp) },
            selected = currentDestination?.route == Routes.You.route,
            onClick = {
                navController.navigate(Routes.You.route) {
                    // Pop up to the start destination of the graph to
                    // avoid building up a large stack of destinations
                    // on the back stack as users select items
                    popUpTo(navController.graph.findStartDestination().id) {
                        saveState = true
                    }
                    launchSingleTop = true
                }
            }
        )
    }
}
