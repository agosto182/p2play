package org.libre.agosto.p2play.ajax

import android.util.JsonReader
import android.util.Log
import java.io.InputStreamReader
import org.libre.agosto.p2play.models.CommentaryModel

class Comments : Client() {
    private fun parseCommentaries(data: JsonReader): ArrayList<CommentaryModel> {
        val commentaries = arrayListOf<CommentaryModel>()
        data.beginObject()
        while (data.hasNext()) {
            when (data.nextName()) {
                "data" -> {
                    data.beginArray()
                    while (data.hasNext()) {
                        val comment = CommentaryModel()
                        comment.parseCommentary(data)
                        commentaries.add(comment)
                    }
                    data.endArray()
                }
                else -> data.skipValue()
            }
        }
        data.endObject()

        return commentaries
    }

    fun getCommentaries(videoId: Int): ArrayList<CommentaryModel> {
        var commentaries = arrayListOf<CommentaryModel>()
        val con = this.newCon("videos/$videoId/comment-threads", "GET")

        try {
            if (con.responseCode == 200) {
                val response = InputStreamReader(con.inputStream)
                val data = JsonReader(response)
                commentaries = parseCommentaries(data)
                data.close()
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }
        con.disconnect()
        return commentaries
    }

    fun makeCommentary(token: String, videoId: Int, text: String): Boolean {
        val con = this.newCon("videos/$videoId/comment-threads", "POST", token)
        val params = "text=$text"
        con.outputStream.write(params.toByteArray())

        var response: Boolean

        try {
            if (con.responseCode == 200) {
                con.disconnect()
                response = true
            } else {
                Log.d("Status", con.responseMessage)
                response = false
            }
        } catch (err: Exception) {
            err.printStackTrace()
            response = false
        }

        con.disconnect()
        return response
    }

    fun getCommentariesThread(videoId: Int, threadId: Int): ArrayList<CommentaryModel> {
        var commentaries = arrayListOf<CommentaryModel>()
        val con = this.newCon("videos/$videoId/comment-threads/$threadId", "GET")

        try {
            if (con.responseCode == 200) {
                val response = InputStreamReader(con.inputStream)
                val data = JsonReader(response)
                data.beginObject()
                while (data.hasNext()) {
                    when (data.nextName()) {
                        "children" -> {
                            data.beginArray()
                            while (data.hasNext()) {
                                data.beginObject()
                                while (data.hasNext()) {
                                    when (data.nextName()) {
                                        "comment" -> {
                                            val comment = CommentaryModel()
                                            comment.parseCommentary(data)
                                            commentaries.add(comment)
                                        }
                                        else -> data.skipValue()
                                    }
                                }
                                data.endObject()
                            }
                            data.endArray()
                        }
                        else -> data.skipValue()
                    }
                }
                data.endObject()
                data.close()
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }
        con.disconnect()
        return commentaries
    }

    fun replyThread(token: String, videoId: Int, threadId: Int, text: String): Boolean {
        val con = this.newCon("videos/$videoId/comments/$threadId", "POST", token)
        val params = "text=$text"
        con.outputStream.write(params.toByteArray())

        val response: Boolean = try {
            if (con.responseCode == 200) {
                con.disconnect()
                true
            } else {
                Log.d("Status", con.responseMessage)
                false
            }
        } catch (err: Exception) {
            err.printStackTrace()
            false
        }

        con.disconnect()
        return response
    }
}
