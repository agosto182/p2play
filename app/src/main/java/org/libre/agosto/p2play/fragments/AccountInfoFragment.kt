package org.libre.agosto.p2play.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import java.io.Serializable
import org.libre.agosto.p2play.databinding.FragmentChannelInfoBinding
import org.libre.agosto.p2play.models.AccountModel

private const val ARG_PARAM1 = "account"

class AccountInfoFragment : Fragment() {
    private var _binding: FragmentChannelInfoBinding? = null
    private val binding get() = _binding!!
    private var account: AccountModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            account = it.getSerializable(ARG_PARAM1) as AccountModel
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentChannelInfoBinding.inflate(inflater, container, false)

        binding.account.text = account?.name
        binding.host.text = account?.host
        binding.description.text = account?.description

        return binding.root
    }

    companion object {

        @JvmStatic
        fun newInstance(account: AccountModel) = AccountInfoFragment().apply {
            arguments = Bundle().apply {
                val param = account as Serializable
                putSerializable(ARG_PARAM1, param)
            }
        }
    }
}
