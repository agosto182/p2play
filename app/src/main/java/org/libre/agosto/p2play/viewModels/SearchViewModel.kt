package org.libre.agosto.p2play.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.libre.agosto.p2play.ajax.Videos
import org.libre.agosto.p2play.models.VideoModel

class SearchViewModel(savedStateHandle: SavedStateHandle) : ViewModel() {
    private val client = Videos()

    private var canLoadMore = true

    private val _videos = MutableLiveData<List<VideoModel>>()
    val videos: LiveData<List<VideoModel>> = _videos

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _query = MutableLiveData<String>()
    val query: LiveData<String> = _query

    fun loadVideos() {
        if (!canLoadMore || isLoading.value == true) {
            return
        }
        _isLoading.value = true
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                client.search(query.value!!, videos.value?.size ?: 0)
            }
            val data = if (videos.value !== null) {
                ArrayList(videos.value!!)
            } else {
                ArrayList()
            }
            if (result.isNotEmpty()) {
                data.addAll(result)
            } else {
                canLoadMore = false
            }

            _videos.postValue(data)
            _isLoading.value = false
        }
    }

    fun onSearch(query: String) {
        _query.value = query
        refresh()
    }

    fun refresh() {
        _videos.value = arrayListOf()
        canLoadMore = true
        loadVideos()
    }
}
