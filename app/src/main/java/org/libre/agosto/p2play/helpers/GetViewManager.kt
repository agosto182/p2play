package org.libre.agosto.p2play.helpers

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.LayoutManager

fun getViewManager(context: Context, resources: Resources): LayoutManager {
    val screenSize = resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK
    val manager = if (screenSize > Configuration.SCREENLAYOUT_SIZE_LARGE) {
        val orientation = resources.configuration.orientation
        val gridItems = when (orientation) {
            Configuration.ORIENTATION_LANDSCAPE -> 4
            else -> 3
        }
        GridLayoutManager(context, gridItems)
    } else {
        LinearLayoutManager(context)
    }

    return manager
}
