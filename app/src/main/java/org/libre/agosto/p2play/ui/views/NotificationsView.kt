package org.libre.agosto.p2play.ui.views

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import org.libre.agosto.p2play.ui.lists.NotificationList
import org.libre.agosto.p2play.viewModels.NotificationsViewModel

@Composable
fun NotificationsView(notificationsViewModel: NotificationsViewModel) {
    val notifications by notificationsViewModel.notifications.observeAsState(listOf())

    LaunchedEffect(Unit) {
        notificationsViewModel.refresh()
    }

    NotificationList(
        notifications,
        { notificationsViewModel.refresh() },
        { notificationsViewModel.loadMore() },
        { notificationsViewModel.onNotificationClicked(it) }
    )
}
