package org.libre.agosto.p2play.ui.components.organisms

import android.content.Intent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import coil3.compose.AsyncImage
import org.libre.agosto.p2play.ChannelActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.ReproductorActivity
import org.libre.agosto.p2play.activities.ui.theme.AppTypography
import org.libre.agosto.p2play.helpers.mapSeconds
import org.libre.agosto.p2play.models.VideoModel

@Composable
fun VideoItem(videoData: VideoModel) {
    val context = LocalContext.current
    val userImgSource = if (videoData.userImageUrl !== "") {
        "https://${ManagerSingleton.url}${videoData.userImageUrl}"
    } else {
        R.drawable.default_avatar
    }

    Card({
        val intent = Intent(context, ReproductorActivity::class.java)
        intent.putExtra("video", videoData)
        context.startActivity(intent)
    }) {
        ConstraintLayout {
            val (thumbnailImg, durationTxt, titleTxt, infoBox, userImg, liveTag) = createRefs()
            Box(
                modifier = Modifier.fillMaxWidth().aspectRatio(16f / 9f).constrainAs(thumbnailImg) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
            ) {
                AsyncImage(
                    "https://${ManagerSingleton.url}${videoData.thumbUrl}",
                    videoData.name,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier.matchParentSize()
                )
            }

            if (videoData.isLive) {
                Text(
                    stringResource(R.string.is_live_video).uppercase(),
                    color = Color.White,
                    modifier = Modifier.alpha(
                        0.6F
                    ).padding(
                        8.dp
                    ).background(Color.Red).padding(horizontal = 2.dp).constrainAs(liveTag) {
                        start.linkTo(thumbnailImg.start)
                        top.linkTo(thumbnailImg.top)
                    }
                )
            } else {
                Text(
                    mapSeconds(videoData.duration.toInt()),
                    color = Color.Black,
                    modifier = Modifier.alpha(
                        0.6F
                    ).padding(5.dp).background(Color.White).constrainAs(durationTxt) {
                        end.linkTo(thumbnailImg.end)
                        bottom.linkTo(thumbnailImg.bottom)
                    }
                )
            }

            Box(
                modifier = Modifier.constrainAs(userImg) {
                    top.linkTo(thumbnailImg.bottom)
                    start.linkTo(parent.start)
                    bottom.linkTo(parent.bottom)
                }.clickable {
                    val intent = Intent(context, ChannelActivity::class.java)
                    intent.putExtra("channel", videoData.getChannel())
                    context.startActivity(intent)
                }
            ) {
                AsyncImage(
                    userImgSource,
                    "Profile photo",
                    contentScale = ContentScale.Fit,
                    modifier = Modifier.padding(10.dp).size(40.dp).clip(CircleShape)
                )
            }

            Text(
                videoData.name,
                style = AppTypography.titleMedium,
                overflow = TextOverflow.Ellipsis,
                lineHeight = 19.sp,
                textAlign = TextAlign.Left,
                modifier = Modifier.constrainAs(titleTxt) {
                    top.linkTo(thumbnailImg.bottom)
                    start.linkTo(userImg.end)
                }.padding(top = 12.dp, end = 70.dp)
            )

            Row(
                modifier = Modifier.constrainAs(infoBox) {
                    top.linkTo(titleTxt.bottom)
                    start.linkTo(userImg.end)
                }.padding(bottom = 5.dp)
            ) {
                Text(videoData.nameChannel, style = AppTypography.labelSmall)
                Text(" - ", style = AppTypography.labelSmall)
                Text(
                    "${videoData.views} ${stringResource(R.string.view_text)}",
                    style = AppTypography.labelSmall
                )
            }

            // createVerticalChain(thumbailImg, titleTxt, infoBox, chainStyle = ChainStyle.Packed)
        }
    }
}
