package org.libre.agosto.p2play.ajax

import android.util.JsonReader
import java.io.InputStreamReader
import org.libre.agosto.p2play.models.ChannelModel

class Subscriptions : Client() {
    fun get(token: String, start: Int = 0, count: Int = 10): ArrayList<ChannelModel> {
        val params = "start=$start&count=$count"
        val con = this.newCon("users/me/subscriptions?$params", "GET", token)
        val channels = arrayListOf<ChannelModel>()

        try {
            if (con.responseCode == 200) {
                val response = InputStreamReader(con.inputStream)
                val data = JsonReader(response)
                data.beginObject()
                while (data.hasNext()) {
                    when (data.nextName()) {
                        "data" -> {
                            data.beginArray()
                            while (data.hasNext()) {
                                val channel = ChannelModel()
                                channel.parseChannel(data)
                                channels.add(channel)
                            }
                            data.endArray()
                        }
                        else -> data.skipValue()
                    }
                }
                data.endObject()
                data.close()
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }

        return channels
    }
}
