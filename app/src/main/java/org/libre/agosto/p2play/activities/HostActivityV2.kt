package org.libre.agosto.p2play.activities

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstraintLayout
import org.libre.agosto.p2play.MainActivityOld
import org.libre.agosto.p2play.activities.ui.theme.P2playTheme

class HostActivityV2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        setContent {
            P2playTheme {
                Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
                    ConstraintLayout(modifier = Modifier.padding(innerPadding).fillMaxSize()) {
                        val (textLabel, emailField, passwordField) = createRefs()
                        Text(
                            "Iniciar session",
                            modifier = Modifier.constrainAs(textLabel) {
                                top.linkTo(parent.top)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                                bottom.linkTo(emailField.top)
                            }
                        )
                        TextField(
                            "",
                            {},
                            label = { Text("Email") },
                            modifier = Modifier.padding(10.dp).constrainAs(emailField) {
                                top.linkTo(textLabel.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                                bottom.linkTo(passwordField.top)
                            }
                        )
                        TextField(
                            "",
                            {},
                            label = { Text("Password") },
                            modifier = Modifier.constrainAs(passwordField) {
                                top.linkTo(emailField.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                                bottom.linkTo(parent.bottom)
                            }
                        )

                        createVerticalChain(
                            textLabel,
                            emailField,
                            passwordField,
                            chainStyle = ChainStyle.Packed
                        )
                    }
                }
            }
        }
    }

    private fun instanceSelected() {
    }

    private fun startApp() {
        runOnUiThread {
            val intent = Intent(this, MainActivityOld::class.java)
            startActivity(intent)
            this.finish()
        }
    }
}
