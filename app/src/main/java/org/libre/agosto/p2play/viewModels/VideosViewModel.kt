package org.libre.agosto.p2play.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.ajax.Videos
import org.libre.agosto.p2play.domain.data.VideoFilterEnum
import org.libre.agosto.p2play.models.VideoModel

class VideosViewModel : ViewModel() {
    val client = Videos()

    var canLoadMore = true

    private val _videos = MutableLiveData<List<VideoModel>>()
    val videos: LiveData<List<VideoModel>> = _videos

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _videoFilter = MutableLiveData<VideoFilterEnum>()
    val videoFilter: LiveData<VideoFilterEnum> = _videoFilter

    fun loadVideos() {
        if (!canLoadMore || isLoading.value == true) {
            return
        }
        _isLoading.value = true
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                getVideoResource()
            }
            val data = if (videos.value !== null) {
                ArrayList(videos.value!!)
            } else {
                ArrayList()
            }

            if (result.isNotEmpty()) {
                data.addAll(result)
            } else {
                canLoadMore = false
            }

            _videos.postValue(data)
            _isLoading.value = false
        }
    }

    fun changeCategory(category: VideoFilterEnum) {
        _videoFilter.value = category
        refresh()
    }

    fun refresh() {
        _videos.value = arrayListOf()
        canLoadMore = true
        loadVideos()
    }

    private fun getVideoResource(): ArrayList<VideoModel> {
        val skip = videos.value?.size ?: 0
        return when (videoFilter.value) {
            VideoFilterEnum.TRENDING -> {
                return client.getTrendingVideos(skip)
            }
            VideoFilterEnum.POPULAR -> {
                client.getPopularVideos(skip)
            }
            VideoFilterEnum.LIKES -> {
                client.getMostLikedVideos(skip)
            }
            VideoFilterEnum.RECENT -> {
                client.getLastVideos(skip)
            }
            VideoFilterEnum.LOCAL -> {
                client.getLocalVideos(skip)
            }
            VideoFilterEnum.HISTORY -> {
                client.videoHistory(ManagerSingleton.token.token, skip)
            }
            VideoFilterEnum.MY_VIDEOS -> {
                client.myVideos(ManagerSingleton.token.token, skip)
            }
            VideoFilterEnum.HOT -> {
                client.getHotVideos(skip)
            }
            else -> {
                client.getHotVideos(skip)
            }
        }
    }
}
