package org.libre.agosto.p2play

import android.content.Context
import android.content.SharedPreferences
import org.libre.agosto.p2play.models.TokenModel
import org.libre.agosto.p2play.models.UserModel

object ManagerSingleton {
    var url: String? = null
    var user: UserModel = UserModel()
    var token: TokenModel = TokenModel()
    var nsfw: Boolean = false
    var videosCount: Int = 0
    lateinit var settings: SharedPreferences
    lateinit var db: Database

    fun toast(text: String?, context: Context) {
        android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT).show()
    }

    fun logout() {
        db.logout()
        user = UserModel()
        token = TokenModel()

        // TODO: Close the session in the user instance
    }

    fun reloadSettings() {
        val host = settings.getString("hostP2play", "")
        val lastHost = settings.getString("last_host", "")
        if (host != "") {
            if (lastHost != host) {
                logout()
            }
            url = host
        }

        nsfw = settings.getBoolean("show_nsfw", false)
        videosCount = settings.getString("videos_count", "15")!!.toInt()
    }

    fun isLogged(): Boolean = token.token !== ""
}
