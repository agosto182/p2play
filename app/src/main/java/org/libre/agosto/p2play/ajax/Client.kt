package org.libre.agosto.p2play.ajax

import android.util.JsonReader
import android.util.Log
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.models.HostModel

open class Client {
    protected fun newCon(
        uri: String,
        method: String,
        token: String? = null,
        isJson: Boolean = false
    ): HttpURLConnection {
        val url = URL("https://${ManagerSingleton.url}/api/v1/$uri")
        val con = url.openConnection() as HttpURLConnection

        con.setRequestProperty("User-Agent", "P2play/0.5.3")
        if (isJson) {
            con.setRequestProperty("Content-Type", "application/json")
        } else {
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        }
        con.setRequestProperty("Accept", "*/*")

        if (token !== null) {
            con.setRequestProperty("Authorization", "Bearer $token")
        }

        con.requestMethod = method
        con.connectTimeout = 60000
        con.readTimeout = 60000

        if (method == "POST") {
            con.doOutput = true
        }

        Log.d("Petition", url.toString())
        return con
    }

    fun getKeys(): HostModel {
        val con = this.newCon("oauth-clients/local", "GET")
        val keys = HostModel("", "")
        try {
            if (con.responseCode == 200) {
                val response = InputStreamReader(con.inputStream)
                val data = JsonReader(response)
                data.beginObject()
                while (data.hasNext()) {
                    val key = data.nextName()
                    when (key.toString()) {
                        "client_id" -> {
                            keys.client_id = data.nextString()
                        }
                        "client_secret" -> {
                            keys.client_secret = data.nextString()
                        }
                        else -> {
                            data.skipValue()
                        }
                    }
                }
                data.close()
            }
        } catch (err: Exception) {
            err.printStackTrace()
        }

        con.disconnect()
        return keys
    }
}
