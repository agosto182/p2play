package org.libre.agosto.p2play.ui

import androidx.navigation.NamedNavArgument
import org.libre.agosto.p2play.R

sealed class Routes(
    val route: String,
    val title: Int?,
    arguments: List<NamedNavArgument> = emptyList(),
    val hasTopAppBar: Boolean = false
) {
    data object Videos : Routes("videos", R.string.app_name)
    data object Subscriptions : Routes("subscriptions", R.string.title_subscriptions)
    data object Search : Routes("search", null)
    data object You : Routes("you", R.string.you)
    data object VideoList : Routes("video-list?key={key}", null, hasTopAppBar = true) {
        fun nav(key: String): String = "video-list?key=$key"
    }
    data object Notifications : Routes("notifications", R.string.app_name)

    companion object {
        private val entries: List<Routes> = listOf(
            Videos,
            Subscriptions,
            Search,
            You,
            VideoList,
            Notifications
        )
        private val routeMap = entries.associateBy { it.route }

        fun fromRoute(route: String?): Routes? {
            if (route == null) {
                return null
            }

            return routeMap[route]
        }
    }
}
