package org.libre.agosto.p2play.helpers

fun mapSeconds(inputSeconds: Int): String {
    val seconds = (inputSeconds % 60)
    var minutes = inputSeconds / 60
    var hours = 0

    if (minutes > 60) {
        hours = minutes / 60
        minutes %= 60
    }

    var result = minutes.toString().padStart(2, '0') + ":" + seconds.toString().padStart(2, '0')

    if (hours > 0) {
        result = hours.toString().padStart(2, '0') + ":" + result
    }

    return result
}
