package org.libre.agosto.p2play.ui.bars

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import org.libre.agosto.p2play.R

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun NotificationsBar(navController: NavController, onMarkAllAsRead: (() -> Unit)? = null) {
    TopAppBar(
        { Text(stringResource(R.string.notification_title)) },
        navigationIcon = {
            IconButton({
                navController.popBackStack()
            }) {
                Icon(Icons.AutoMirrored.Default.ArrowBack, null)
            }
        },
        actions = {
            IconButton({
                if (onMarkAllAsRead !== null) {
                    onMarkAllAsRead()
                }
            }) {
                // Import Drawable icon with double check mark
                Icon(painterResource(R.drawable.baseline_check_24), null)
            }
        }
    )
}
