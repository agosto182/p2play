package org.libre.agosto.p2play.helpers

import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.grid.LazyGridState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.snapshotFlow

@Composable
fun InfiniteScrollHandler(lazyState: LazyListState, buffer: Int = 1, onLoadMore: () -> Unit) {
    LaunchedEffect(lazyState) {
        snapshotFlow { lazyState.layoutInfo.visibleItemsInfo }
            .collect { visibleItems ->
                val items = lazyState.layoutInfo.totalItemsCount
                if (visibleItems.isNotEmpty() &&
                    items > buffer &&
                    visibleItems.last().index >= items - 1
                ) {
                    onLoadMore()
                }
            }
    }
}

@Composable
fun InfiniteScrollHandler(lazyState: LazyGridState, buffer: Int = 1, onLoadMore: () -> Unit) {
    LaunchedEffect(lazyState) {
        snapshotFlow { lazyState.layoutInfo.visibleItemsInfo }
            .collect { visibleItems ->
                val items = lazyState.layoutInfo.totalItemsCount
                if (visibleItems.isNotEmpty() &&
                    items > buffer &&
                    visibleItems.last().index >= items - 1
                ) {
                    onLoadMore()
                }
            }
    }
}
