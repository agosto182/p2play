package org.libre.agosto.p2play.ui.components.molecules

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.ui.PlayerView

@Composable
fun Player(playerProvider: () -> ExoPlayer?, modifier: Modifier = Modifier) {
    val context = LocalContext.current
    val currentPlayer by rememberUpdatedState(newValue = playerProvider())

    AndroidView(factory = {
        PlayerView(context).apply {
            // setControllerLayoutId(R.layout.custom_player_controls)
            player = currentPlayer
            useController = false
        }
    }, modifier, {
        it.player = currentPlayer
    })
}
