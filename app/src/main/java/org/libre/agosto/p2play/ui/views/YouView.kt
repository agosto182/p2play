package org.libre.agosto.p2play.ui.views

import android.content.Intent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ExitToApp
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.HorizontalDivider
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.compose.LocalLifecycleOwner
import androidx.navigation.NavController
import org.libre.agosto.p2play.AboutActivity
import org.libre.agosto.p2play.LoginActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.SettingsActivity2
import org.libre.agosto.p2play.domain.data.VideoFilterEnum
import org.libre.agosto.p2play.ui.Routes
import org.libre.agosto.p2play.ui.components.molecules.AccountBanner

@Composable
fun YouView(navController: NavController) {
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current
    val logoutMsg = stringResource(R.string.logout_msg)

    var isLogged by remember { mutableStateOf(ManagerSingleton.isLogged()) }

    DisposableEffect(lifecycleOwner) {
        val observer = LifecycleEventObserver { _, _ ->
            isLogged = ManagerSingleton.isLogged()
        }

        lifecycleOwner.lifecycle.addObserver(observer)

        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    Column {
        AccountBanner(ManagerSingleton.user.username, ManagerSingleton.user.avatar)

        if (isLogged) {
            ListItem(
                headlineContent = { Text(stringResource(R.string.title_myVideos)) },
                leadingContent = {
                    Icon(
                        painterResource(R.drawable.ic_menu_gallery),
                        contentDescription = null
                    )
                },
                modifier = Modifier.clickable {
                    navController.navigate(
                        Routes.VideoList.nav(VideoFilterEnum.MY_VIDEOS.toString())
                    )
                }
            )
            ListItem(
                headlineContent = { Text(stringResource(R.string.nav_history)) },
                leadingContent = {
                    Icon(
                        painterResource(R.drawable.ic_history_black_24dp),
                        contentDescription = null
                    )
                },
                modifier = Modifier.clickable {
                    navController.navigate(Routes.VideoList.nav(VideoFilterEnum.HISTORY.toString()))
                }
            )

            HorizontalDivider()

            ListItem(
                headlineContent = { Text(stringResource(R.string.action_logout)) },
                leadingContent = {
                    Icon(
                        Icons.AutoMirrored.Default.ExitToApp,
                        contentDescription = null
                    )
                },
                modifier = Modifier.clickable {
                    ManagerSingleton.logout()
                    ManagerSingleton.toast(logoutMsg, context)
                    isLogged = ManagerSingleton.isLogged()
                }
            )
        } else {
            ListItem(
                headlineContent = { Text(stringResource(R.string.action_login)) },
                leadingContent = {
                    Icon(
                        Icons.Default.AccountCircle,
                        contentDescription = null
                    )
                },
                modifier = Modifier.clickable {
                    val intent = Intent(context, LoginActivity::class.java)
                    context.startActivity(intent)
                }
            )
        }

        ListItem(
            headlineContent = { Text(stringResource(R.string.action_settings)) },
            leadingContent = {
                Icon(
                    painterResource(R.drawable.ic_baseline_settings_24),
                    contentDescription = null
                )
            },
            modifier = Modifier.clickable {
                val intent = Intent(context, SettingsActivity2::class.java)
                context.startActivity(intent)
            }
        )
        ListItem(
            headlineContent = { Text(stringResource(R.string.nav_about)) },
            leadingContent = {
                Icon(
                    painterResource(R.drawable.ic_info_black_24dp),
                    contentDescription = null
                )
            },
            modifier = Modifier.clickable {
                val intent = Intent(context, AboutActivity::class.java)
                context.startActivity(intent)
            }
        )
    }
}
