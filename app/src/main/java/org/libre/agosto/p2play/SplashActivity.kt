package org.libre.agosto.p2play

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import java.lang.Exception
import org.libre.agosto.p2play.activities.MainActivity
import org.libre.agosto.p2play.ajax.Auth
import org.libre.agosto.p2play.helpers.TaskManager
import org.libre.agosto.p2play.models.TokenModel

class SplashActivity : AppCompatActivity() {
    private lateinit var settings: SharedPreferences
    private val client: Auth = Auth()
    private val db = Database(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        settings = PreferenceManager.getDefaultSharedPreferences(this)
        ManagerSingleton.settings = settings
        ManagerSingleton.db = db

        ManagerSingleton.reloadSettings()

        val host = settings.getString("hostP2play", "")
        val lastHost = settings.getString("last_host", "")
        if (host != "") {
            if (lastHost != host) {
                Thread.sleep(2000)
                startHostActivity()
            } else {
                ManagerSingleton.url = host
                checkUser()
            }
        } else {
            Thread.sleep(2000)
            startHostActivity()
        }
    }

    private fun checkUser() {
        try {
            val token = db.getToken()
            val user = db.getUser()

            if (token.status == 1 && user.status == 1) {
                val clientId = settings.getString("client_id", "")!!
                val clientSecret = settings.getString("client_secret", "")!!
                val task = TaskManager<TokenModel>()

                task.runTask(
                    {
                        client.refreshToken(token, clientId, clientSecret)
                    },
                    {
                        when (it.status.toString()) {
                            "1" -> {
                                db.newToken(it)
                                ManagerSingleton.token = it
                                ManagerSingleton.user = user
                            }
                            else -> ManagerSingleton.logout()
                        }
                        startApp()
                    }
                )
            } else {
                ManagerSingleton.logout()
                startApp()
            }
        } catch (err: Exception) {
            err.printStackTrace()
            Thread.sleep(2000)
            startHostActivity()
        }
    }

    private fun startApp() {
        val useOldInterface = settings.getBoolean("use_old_interface", false)
        runOnUiThread {
            val intent = if (useOldInterface) {
                Intent(this, MainActivityOld::class.java)
            } else {
                Intent(this, MainActivity::class.java)
            }
            startActivity(intent)
            this.finish()
        }
    }

    private fun startHostActivity() {
        runOnUiThread {
            val intent = Intent(this, HostActivity::class.java)
            startActivity(intent)
            this.finish()
        }
    }
}
