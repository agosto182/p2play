package org.libre.agosto.p2play.ui.bars

import android.content.Intent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.MoreVert
import androidx.compose.material.icons.filled.Notifications
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Badge
import androidx.compose.material3.BadgedBox
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import org.libre.agosto.p2play.AboutActivity
import org.libre.agosto.p2play.LoginActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.SettingsActivity2
import org.libre.agosto.p2play.models.NotificationModel
import org.libre.agosto.p2play.ui.Routes

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainTopAppBar(
    navController: NavController,
    modifier: Modifier = Modifier,
    notifications: List<NotificationModel> = emptyList()
) {
    var isMenuOpen by remember { mutableStateOf(false) }
    val context = LocalContext.current

    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    val title = when (currentDestination?.route) {
        Routes.Subscriptions.route -> stringResource(Routes.Subscriptions.title!!)
        else -> stringResource(R.string.app_name)
    }

    TopAppBar(
        { Text(title) },
        modifier,
        actions = {
            IconButton({
                navController.navigate(Routes.Search.route) {
                    launchSingleTop = true
                }
            }) { Icon(Icons.Default.Search, null) }

            if (ManagerSingleton.isLogged()) {
                BadgedBox({
                    if (notifications.isNotEmpty()) {
                        Badge {
                            Text(notifications.size.toString())
                        }
                    }
                }) {
                    IconButton({
                        navController.navigate(Routes.Notifications.route) {
                            launchSingleTop = true
                            restoreState = true
                        }
                    }) { Icon(Icons.Default.Notifications, null) }
                }
            }

            IconButton({ isMenuOpen = true }) {
                Icon(Icons.Default.MoreVert, "More")
                DropdownMenu(isMenuOpen, { isMenuOpen = false }) {
                    if (ManagerSingleton.isLogged()) {
                        DropdownMenuItem({ Text(stringResource(R.string.action_logout)) }, {
                            isMenuOpen = false
                            ManagerSingleton.logout()
                        })
                    } else {
                        DropdownMenuItem({ Text(stringResource(R.string.action_login)) }, {
                            isMenuOpen = false
                            val intent = Intent(context, LoginActivity::class.java)
                            context.startActivity(intent)
                        })
                    }
                    DropdownMenuItem({ Text(stringResource(R.string.action_settings)) }, {
                        isMenuOpen = false
                        val intent = Intent(context, SettingsActivity2::class.java)
                        context.startActivity(intent)
                    })
                    DropdownMenuItem({ Text(stringResource(R.string.nav_about)) }, {
                        isMenuOpen = false
                        val intent = Intent(context, AboutActivity::class.java)
                        context.startActivity(intent)
                    })
                }
            }
        }
    )
}
