package org.libre.agosto.p2play.ui.components.molecules

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import coil3.compose.AsyncImage
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R

@Composable
fun AccountBanner(displayName: String, img: Any? = null, modifier: Modifier = Modifier) {
    // val context = LocalContext.current
    val userImgSource = if (img !== null) {
        "https://${ManagerSingleton.url}$img"
    } else {
        R.drawable.default_avatar
    }
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        AsyncImage(
            userImgSource,
            contentDescription = null,
            modifier = Modifier
                .size(50.dp)
                .clip(CircleShape),
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.width(16.dp))
        Text(
            text = displayName,
            style = MaterialTheme.typography.titleLarge,
            modifier = Modifier.weight(1f).clickable {
//                val intent = Intent(context, AccountActivity::class.java)
//                intent.putExtra("accountId", account.getAccount())
//                context.startActivity(intent)
            }
        )
    }
}
