package org.libre.agosto.p2play.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import java.io.Serializable
import org.libre.agosto.p2play.ChannelActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.ReproductorActivity
import org.libre.agosto.p2play.helpers.mapSeconds
import org.libre.agosto.p2play.models.VideoModel

class VideosAdapter(private val myDataset: ArrayList<VideoModel>) :
    RecyclerView.Adapter<VideosAdapter.ViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        val thumb: ImageView = view.findViewById(R.id.thumb)
        val userImg: ImageView = view.findViewById(R.id.userImg)
        val title: TextView = view.findViewById(R.id.tittleTxt)
        val description: TextView = view.findViewById(R.id.descriptionTxt)
        val context: Context = view.context
        val duration: TextView = view.findViewById(R.id.duration)
        val isLive: TextView = view.findViewById(R.id.isLive)
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_video, parent, false) as View
        // set the view's size, margins, paddings and layout parameters
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.title.text = myDataset[position].name
        holder.title.setOnClickListener {
            this.launchChannelActivity(myDataset[position] as Serializable, holder.context)
        }
        Picasso.get().load(
            "https://" + ManagerSingleton.url + myDataset[position].thumbUrl
        ).into(holder.thumb)
        holder.thumb.setOnClickListener {
            this.launchChannelActivity(myDataset[position] as Serializable, holder.context)
        }

        holder.userImg.setOnClickListener {
            val intent = Intent(holder.context, ChannelActivity::class.java)
            intent.putExtra("channel", myDataset[position].getChannel())
            holder.context.startActivity(intent)
        }

        if (myDataset[position].userImageUrl != "") {
            Picasso.get().load(
                "https://" + ManagerSingleton.url + myDataset[position].userImageUrl
            ).into(holder.userImg)
        } else {
            Picasso.get().load(R.drawable.default_avatar).into(holder.userImg)
        }

        val viewsText = holder.context.getString(R.string.view_text)
        val seconds = myDataset[position].duration.toInt()
        val timeString = mapSeconds(seconds)

        holder.duration.text = timeString

        holder.description.text =
            myDataset[position].username + " - " + myDataset[position].views + " " + viewsText

        if (myDataset[position].isLive) {
            holder.isLive.visibility = View.VISIBLE
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataset.size

    fun clearData() {
        myDataset.clear()
        notifyDataSetChanged()
    }

    fun addData(newItems: ArrayList<VideoModel>) {
        val lastPos = myDataset.size
        myDataset.addAll(newItems)
        notifyItemRangeInserted(lastPos, newItems.size)
    }

    private fun launchChannelActivity(data: Serializable, context: Context) {
        val intent = Intent(context, ReproductorActivity::class.java)
        intent.putExtra("video", data)
        context.startActivity(intent)
    }
}
