package org.libre.agosto.p2play.activities.ui.theme

import androidx.compose.material3.Typography

val AppTypography = Typography()
