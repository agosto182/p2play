package org.libre.agosto.p2play.ui.views

import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.domain.data.VideoFilterEnum
import org.libre.agosto.p2play.ui.lists.VideoList
import org.libre.agosto.p2play.viewModels.VideosViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun VideoListView(
    videosViewModel: VideosViewModel,
    videoFilter: String,
    navController: NavController,
    modifier: Modifier = Modifier
) {
    val title = when (videoFilter) {
        VideoFilterEnum.MY_VIDEOS.toString() -> stringResource(R.string.title_myVideos)
        VideoFilterEnum.HISTORY.toString() -> stringResource(R.string.nav_history)
        else -> ""
    }

    val videos by videosViewModel.videos.observeAsState(initial = listOf())
    val isLoading: Boolean by videosViewModel.isLoading.observeAsState(initial = false)

    LaunchedEffect(Unit) {
        if (videos.isEmpty()) {
            videosViewModel.changeCategory(VideoFilterEnum.valueOf(videoFilter))
        }
    }

    Column {
        TopAppBar(
            {
                Text(title)
            },
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack() }) {
                    Icon(Icons.AutoMirrored.Default.ArrowBack, contentDescription = null)
                }
            },
            colors = TopAppBarDefaults.topAppBarColors(
                containerColor = MaterialTheme.colorScheme.surface
            )
            // windowInsets = WindowInsets(top = 20.dp),
            // modifier = Modifier.height(64.dp)
        )
        VideoList(
            ArrayList(videos),
            isLoading = isLoading,
            onRefresh = {
                if (!isLoading) {
                    videosViewModel.refresh()
                }
            },
            onLoadMore = {
                if (!isLoading) {
                    videosViewModel.loadVideos()
                }
            }
        )
    }
}
