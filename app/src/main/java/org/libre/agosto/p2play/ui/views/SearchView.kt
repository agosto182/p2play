package org.libre.agosto.p2play.ui.views

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import java.util.ArrayList
import org.libre.agosto.p2play.ui.lists.VideoList
import org.libre.agosto.p2play.viewModels.SearchViewModel

@Composable
fun SearchView(searchViewModel: SearchViewModel, modifier: Modifier = Modifier) {
    val videos by searchViewModel.videos.observeAsState(initial = listOf())
    val isLoading: Boolean by searchViewModel.isLoading.observeAsState(initial = false)

    VideoList(
        ArrayList(videos),
        { },
        isLoading = isLoading,
        onRefresh = {
            if (!isLoading) {
                searchViewModel.refresh()
            }
        },
        onLoadMore = {
            if (!isLoading) {
                searchViewModel.loadVideos()
            }
        }
    )
}
