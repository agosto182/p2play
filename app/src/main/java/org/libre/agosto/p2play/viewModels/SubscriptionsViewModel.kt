package org.libre.agosto.p2play.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.ajax.Subscriptions
import org.libre.agosto.p2play.ajax.Videos
import org.libre.agosto.p2play.models.ChannelModel
import org.libre.agosto.p2play.models.VideoModel

class SubscriptionsViewModel(savedStateHandle: SavedStateHandle) : ViewModel() {
    val client = Videos()
    val clientSubscriptions = Subscriptions()

    private val _videos = MutableLiveData<List<VideoModel>>()
    val videos: LiveData<List<VideoModel>> = _videos

    private val _subscriptions = MutableLiveData<List<ChannelModel>>()
    val subscriptions: LiveData<List<ChannelModel>> = _subscriptions

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun loadVideos() {
        _isLoading.value = true
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                client.videoSubscriptions(ManagerSingleton.token.token, videos.value?.size ?: 0)
            }
            val data = if (videos.value !== null) {
                ArrayList(videos.value!!)
            } else {
                ArrayList()
            }
            data.addAll(result)
            _videos.postValue(data)
            _isLoading.value = false
        }
    }

    fun loadSubscriptions() {
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                clientSubscriptions.get(ManagerSingleton.token.token)
            }
            _subscriptions.postValue(result)
        }
    }

    fun refresh() {
        _videos.value = arrayListOf()
        loadVideos()
    }
}
