package org.libre.agosto.p2play.ui.components.molecules

import android.content.Intent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil3.compose.AsyncImage
import org.libre.agosto.p2play.ChannelActivity
import org.libre.agosto.p2play.ManagerSingleton
import org.libre.agosto.p2play.R
import org.libre.agosto.p2play.models.ChannelModel

@Composable
fun SubscriptionIcon(videoChannel: ChannelModel) {
    val context = LocalContext.current
    val userImgSource = if (videoChannel.channelImg !== "") {
        "https://${ManagerSingleton.url}${videoChannel.channelImg}"
    } else {
        R.drawable.default_avatar
    }
    AsyncImage(
        userImgSource,
        videoChannel.name,
        modifier = Modifier
            .padding(horizontal = 5.dp)
            .size(45.dp)
            .clip(CircleShape)
            .clickable {
                val intent = Intent(context, ChannelActivity::class.java)
                intent.putExtra("channel", videoChannel.getFullName())
                context.startActivity(intent)
            }
    )
}
