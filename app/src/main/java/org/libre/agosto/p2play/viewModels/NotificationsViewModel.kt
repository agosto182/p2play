package org.libre.agosto.p2play.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.libre.agosto.p2play.ajax.Notifications
import org.libre.agosto.p2play.models.NotificationModel

class NotificationsViewModel : ViewModel() {
    val client = Notifications()

    private val _notifications = MutableLiveData<List<NotificationModel>>()
    val notifications: LiveData<List<NotificationModel>> = _notifications

    val unreadNotifications = MediatorLiveData<List<NotificationModel>>().apply {
        addSource(_notifications) { list ->
            value = list.filter { !it.read }
        }
    }

    fun loadMore() {
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                client.get(notifications.value?.size ?: 0)
            }
            val data = if (notifications.value !== null) {
                ArrayList(notifications.value!!)
            } else {
                ArrayList()
            }
            data.addAll(result)
            _notifications.postValue(data)
        }
    }

    fun refresh() {
        this._notifications.value = emptyList()
        this.loadMore()
    }

    fun onMarkAllAsRead() {
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                client.markAllAsRead()
            }

            Log.d("result", result.toString())
            refresh()
        }
    }

    fun onNotificationClicked(notificationId: Int) {
        viewModelScope.launch {
            val result = withContext(Dispatchers.IO) {
                client.markAsRead(notificationId)
            }

            Log.d("result", result.toString())
            refresh()
        }
    }
}
