[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/org.libre.agosto.p2play/)

# P2Play

P2Play is an unoficial Peertube android application.

[What is Peertube?](https://github.com/Chocobozzz/PeerTube/)

## Screenshots

<img src="metadata/en-US/images/phoneScreenshots/screen1.png" alt="Screenshot" width="19%"/>
<img src="metadata/en-US/images/phoneScreenshots/screen2.png" alt="Screenshot" width="19%"/>
<img src="metadata/en-US/images/phoneScreenshots/screen3.png" alt="Screenshot" width="19%"/>
<img src="metadata/en-US/images/phoneScreenshots/screen4.png" alt="Screenshot" width="19%"/>
<img src="metadata/en-US/images/phoneScreenshots/screen5.png" alt="Screenshot" width="19%">/
<img src="metadata/en-US/images/phoneScreenshots/screen6.png" alt="Screenshot" width="19%"/>
<img src="metadata/en-US/images/phoneScreenshots/screen7.png" alt="Screenshot" width="19%"/>

## Realeases (apk's)

[All realeases are here](https://gitlab.com/agosto182/p2play/tags)

## Features

- Discover videos
- Play videos
- Login and register in your instance
- Subscribe and manage your subscriptions
- Show your history
- Interact, rate, comment and reply on videos
- Download videos
- Discover profiles, accounts, and more
- Keep updated with your notifications
- Day/Night theme

## Demo

> WIP

## Contact
You can follow our accounts to get news and contact with the developer(s).

- WriteFreely (ActivityPub): https://personaljournal.ca/p2play/

## About
P2Play is made in Android Studio with Kotlin languaje.

### Developers
- Ivan Agosto: [https://mast.lat/@agosto182](https://mast.lat/@agosto182)

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see [license](./LICENSE.md)
